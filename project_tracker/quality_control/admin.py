from django.contrib import admin

from .models import BugReport, FeatureRequest


@admin.register(BugReport)
class BugReportAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'project', 'task', 'status', 'priority', 'created_at', 'updated_at')
    list_filter = ('project', 'status', 'priority')
    search_fields = ('title', 'description')
    list_editable = ('status', 'priority')
    fieldsets = (
        ('Bug info', {
            'fields': ('title', 'description', 'status', 'priority')
        }),
        ('Related Project, Task', {
            'fields': ('project', 'task',)
        }),
    )


@admin.register(FeatureRequest)
class FeatureRequestAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'project', 'task', 'status', 'priority', 'created_at', 'updated_at')
    list_filter = ('project', 'status', 'priority')
    search_fields = ('title', 'description')
    list_editable = ('status', 'priority')
    fieldsets = (
        ('Request info', {
            'fields': ('title', 'description', 'status', 'priority')
        }),
        ('Related Project, Task', {
            'fields': ('project', 'task',)
        }),
    )
