from django.forms import ModelChoiceField
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from tasks.models import Project, Task
from .models import BugReport, FeatureRequest
from .forms import BugReportForm, FeatureRequestForm


def index(request):
    return render(request, 'quality_control/index.html')


class IndexView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'quality_control/index.html')


def bug_list(request):
    bugreport_list = BugReport.objects.all()
    return render(request, 'quality_control/bug_list.html', {'bugreport_list': bugreport_list})


class BugListView(ListView):
    model = BugReport
    template_name = 'quality_control/bug_list.html'


def feature_list(request):
    featurerequest_list = FeatureRequest.objects.all()
    return render(request, 'quality_control/feature_list.html', {'featurerequest_list': featurerequest_list})


class FeatureListView(ListView):
    model = FeatureRequest
    template_name = 'quality_control/feature_list.html'


def bug_detail(request, bug_id):
    bugreport = get_object_or_404(BugReport, pk=bug_id)
    return render(request, 'quality_control/bug_detail.html', {'bugreport': bugreport})


class BugDetailView(DetailView):
    model = BugReport
    pk_url_kwarg = 'bug_id'
    template_name = 'quality_control/bug_detail.html'


def feature_detail(request, feature_id):
    featurerequest = get_object_or_404(FeatureRequest, pk=feature_id)
    return render(request, 'quality_control/feature_detail.html', {'featurerequest': featurerequest})


class FeatureDetailView(DetailView):
    model = FeatureRequest
    pk_url_kwarg = 'feature_id'
    template_name = 'quality_control/feature_detail.html'


def create_bug_report(request):
    if request.method == 'POST':
        form = BugReportForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('quality_control:bug_list')
    else:
        form = BugReportForm()
    return render(request, 'quality_control/bug_report_form.html', {'form': form})


class CreateBugReport(CreateView):
    model = BugReport
    form_class = BugReportForm
    template_name = 'quality_control/bug_report_form.html'

    def get_success_url(self):
        return reverse('quality_control:bug_list')


def create_feature_request(request):
    if request.method == 'POST':
        form = FeatureRequestForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('quality_control:feature_list')
    else:
        form = FeatureRequestForm()
    return render(request, 'quality_control/feature_request_form.html', {'form': form})


class CreateFeatureRequest(CreateView):
    model = FeatureRequest
    form_class = FeatureRequestForm
    template_name = 'quality_control/feature_request_form.html'

    def get_success_url(self):
        return reverse('quality_control:feature_list')


def update_bug_report(request, bug_id):
    bug_report = get_object_or_404(BugReport, pk=bug_id)
    if request.method == 'POST':
        form = BugReportForm(request.POST, instance=bug_report)
        if form.is_valid():
            form.save()
            return redirect('quality_control:bug_detail', bug_id=bug_report.id)
    else:
        form = BugReportForm(instance=bug_report)
    return render(request, 'quality_control/bug_report_update.html', {'form': form, 'bug_report': bug_report})


class UpdateBugReport(UpdateView):
    model = BugReport
    form_class = BugReportForm
    pk_url_kwarg = 'bug_id'
    template_name = 'quality_control/bug_report_update.html'

    def get_success_url(self):
        return reverse('quality_control:bug_detail', kwargs={'bug_id': self.object.id})


def update_feature_request(request, feature_id):
    feature_request = get_object_or_404(FeatureRequest, pk=feature_id)
    if request.method == 'POST':
        form = FeatureRequestForm(request.POST, instance=feature_request)
        if form.is_valid():
            form.save()
            return redirect('quality_control:feature_id_detail', feature_id=feature_request.id)
    else:
        form = FeatureRequestForm(instance=feature_request)
    return render(request, 'quality_control/feature_request_update.html',
                  {'form': form, 'feature_request': feature_request})


class UpdateFeatureRequest(UpdateView):
    model = FeatureRequest
    form_class = FeatureRequestForm
    pk_url_kwarg = 'feature_id'
    template_name = 'quality_control/feature_request_update.html'

    def get_success_url(self):
        return reverse('quality_control:feature_id_detail', kwargs={'feature_id': self.object.id})


def delete_bug_report(request, bug_id):
    bug_report = get_object_or_404(BugReport, pk=bug_id)
    bug_report.delete()
    return redirect('quality_control:bug_list')


class DeleteBugReport(DeleteView):
    model = BugReport
    pk_url_kwarg = 'bug_id'
    success_url = reverse_lazy('quality_control:bug_list')
    template_name = 'quality_control/bug_confirm_delete.html'


def delete_feature_request(request, feature_id):
    feature_request = get_object_or_404(FeatureRequest, pk=feature_id)
    feature_request.delete()
    return redirect('quality_control:feature_list')


class DeleteFeatureRequest(DeleteView):
    model = FeatureRequest
    pk_url_kwarg = 'feature_id'
    success_url = reverse_lazy('quality_control:feature_list')
    template_name = 'quality_control/feature_confirm_delete.html'
