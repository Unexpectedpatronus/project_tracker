from django.urls import path

from quality_control import views

app_name = 'quality_control'

urlpatterns = [
    # path('', views.index, name='index'),
    # path('bugs/', views.bug_list, name='bug_list'),
    # path('features/', views.feature_list, name='feature_list'),
    # path('bugs/<int:bug_id>/', views.bug_detail, name='bug_detail'),
    # path('features/<int:feature_id>/', views.feature_detail, name='feature_id_detail'),
    # path('bugs/bug_report_form/', views.create_bug_report, name='bug_report_form'),
    # path('features/feature_request_form/', views.create_feature_request, name='feature_request_form'),
    # path('bugs/<int:bug_id>/update/', views.update_bug_report, name='update_bug_report'),
    # path('features/<int:feature_id>/update/', views.update_feature_request, name='update_feature_request'),
    # path('bugs/<int:bug_id>/delete/', views.delete_bug_report, name='delete_bug_report'),
    # path('features/<int:feature_id>/delete/', views.delete_feature_request, name='delete_feature_request'),

    path('', views.IndexView.as_view(), name='index'),
    path('bugs/', views.BugListView.as_view(), name='bug_list'),
    path('features/', views.FeatureListView.as_view(), name='feature_list'),
    path('bugs/<int:bug_id>/', views.BugDetailView.as_view(), name='bug_detail'),
    path('features/<int:feature_id>/', views.FeatureDetailView.as_view(), name='feature_id_detail'),
    path('bugs/bug_report_form/', views.CreateBugReport.as_view(), name='bug_report_form'),
    path('features/feature_request_form/', views.CreateFeatureRequest.as_view(), name='feature_request_form'),
    path('bugs/<int:bug_id>/update/', views.UpdateBugReport.as_view(), name='update_bug_report'),
    path('features/<int:feature_id>/update/', views.UpdateFeatureRequest.as_view(), name='update_feature_request'),
    path('bugs/<int:bug_id>/delete/', views.DeleteBugReport.as_view(), name='delete_bug_report'),
    path('features/<int:feature_id>/delete/', views.DeleteFeatureRequest.as_view(), name='delete_feature_request'),

]
